/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('vendings', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    sex: {
      type: DataTypes.ENUM('F','M'),
      allowNull: false,
      defaultValue: "M"
    },
    map: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    x: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    y: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    body_direction: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "4"
    },
    head_direction: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "0"
    },
    sit: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "1"
    },
    autotrade: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'vendings'
  });
};
