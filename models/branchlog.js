/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('branchlog', {
    branch_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    branch_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    account_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    char_name: {
      type: DataTypes.STRING(25),
      allowNull: false,
      defaultValue: ""
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'branchlog'
  });
};
