/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mail', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    send_name: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: ""
    },
    send_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    dest_name: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: ""
    },
    dest_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    title: {
      type: DataTypes.STRING(45),
      allowNull: false,
      defaultValue: ""
    },
    message: {
      type: DataTypes.STRING(500),
      allowNull: false,
      defaultValue: ""
    },
    time: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    zeny: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'mail'
  });
};
