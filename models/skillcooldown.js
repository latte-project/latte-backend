/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('skillcooldown', {
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    skill: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    tick: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'skillcooldown'
  });
};
