/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mob_skill_db2_re', {
    MOB_ID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    INFO: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    STATE: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    SKILL_ID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    SKILL_LV: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    RATE: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    CASTTIME: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    DELAY: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    CANCELABLE: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    TARGET: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    CONDITION: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    CONDITION_VALUE: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    VAL1: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    VAL2: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    VAL3: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    VAL4: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    VAL5: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    EMOTION: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    CHAT: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'mob_skill_db2_re'
  });
};
