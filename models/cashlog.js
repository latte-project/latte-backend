/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cashlog', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.ENUM('T','V','P','M','S','N','D','C','A','E','I','B','$'),
      allowNull: false,
      defaultValue: "S"
    },
    cash_type: {
      type: DataTypes.ENUM('O','K','C'),
      allowNull: false,
      defaultValue: "O"
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'cashlog'
  });
};
