/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('zenylog', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    src_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.ENUM('T','V','P','M','S','N','D','C','A','E','I','B','K'),
      allowNull: false,
      defaultValue: "S"
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'zenylog'
  });
};
