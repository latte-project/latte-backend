/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('guild_storage_log', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    guild_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    },
    nameid: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    identify: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    refine: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    attribute: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card0: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card1: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card2: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card3: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    option_id0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    expire_time: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    unique_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    bound: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'guild_storage_log'
  });
};
