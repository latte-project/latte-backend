/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mercenary_owner', {
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    merc_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    arch_calls: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    arch_faith: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    spear_calls: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    spear_faith: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    sword_calls: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    sword_faith: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'mercenary_owner'
  });
};
