/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('login', {
    account_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userid: {
      type: DataTypes.STRING(23),
      allowNull: false,
      defaultValue: ""
    },
    user_pass: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: ""
    },
    sex: {
      type: DataTypes.ENUM('M','F','S'),
      allowNull: false,
      defaultValue: "M"
    },
    email: {
      type: DataTypes.STRING(39),
      allowNull: false,
      defaultValue: ""
    },
    group_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    state: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    unban_time: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    expiration_time: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    logincount: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    lastlogin: {
      type: DataTypes.DATE,
      allowNull: true
    },
    last_ip: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: ""
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    character_slots: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    pincode: {
      type: DataTypes.STRING(4),
      allowNull: false,
      defaultValue: ""
    },
    pincode_change: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    vip_time: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    old_group: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    web_auth_token: {
      type: DataTypes.STRING(17),
      allowNull: true,
      unique: true
    },
    web_auth_token_enabled: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'login'
  });
};
