/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('atcommandlog', {
    atcommand_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    atcommand_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    char_name: {
      type: DataTypes.STRING(25),
      allowNull: false,
      defaultValue: ""
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    },
    command: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'atcommandlog'
  });
};
