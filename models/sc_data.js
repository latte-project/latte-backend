/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sc_data', {
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    tick: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    val1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    val2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    val3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    val4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'sc_data'
  });
};
