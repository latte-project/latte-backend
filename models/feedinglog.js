/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('feedinglog', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    target_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    target_class: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    type: {
      type: DataTypes.ENUM('P','H','O'),
      allowNull: false
    },
    intimacy: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    item_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false
    },
    x: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    y: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'feedinglog'
  });
};
