/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('friends', {
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    friend_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    }
  }, {
    sequelize,
    tableName: 'friends'
  });
};
