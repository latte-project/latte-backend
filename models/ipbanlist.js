/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ipbanlist', {
    list: {
      type: DataTypes.STRING(15),
      allowNull: false,
      defaultValue: "",
      primaryKey: true
    },
    btime: {
      type: DataTypes.DATE,
      allowNull: false,
      primaryKey: true
    },
    rtime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    reason: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'ipbanlist'
  });
};
