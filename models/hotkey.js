/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hotkey', {
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    hotkey: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    itemskill_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    skill_lvl: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'hotkey'
  });
};
