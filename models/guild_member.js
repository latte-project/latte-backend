/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('guild_member', {
    guild_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    exp: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    position: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'guild_member'
  });
};
