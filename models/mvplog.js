/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mvplog', {
    mvp_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    mvp_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    kill_char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    monster_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    prize: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    mvpexp: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'mvplog'
  });
};
