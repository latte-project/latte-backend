import { IYamlService } from '#types/yaml';
import { injectable } from 'inversify';
import * as yaml from 'js-yaml';
import * as fs from 'fs';

@injectable()
export class YamlService implements IYamlService {
  constructor() {}

  readYamlFile(path: string) {
    try {
      const yamlFile = fs.readFileSync(path, 'utf-8');
      const yamlData = yaml.safeLoad(yamlFile);
      return JSON.parse(JSON.stringify(yamlData));
    } catch (err) {
      throw new Error('readYamlFile error');
    }
  }
}
