import { IUserService, INewAccount, ILoginPayload, IAccessToken, TOKEN_TYPE } from '#types/user';
import { injectable, inject } from 'inversify';
import { ISequelizeService } from '#types/sequelize';
import * as sequelize from 'sequelize';
import * as mongoose from 'mongoose';
import { IJwtService } from '#types/jwt';
import userModel from '#models/user.model';
import tokenModel from '#models/token.model';
import { accessEnv } from '#helpers/accessEnv';
import refreshTokenModel from '#models/refresh-token.model';
const loginModel = require('#db/legacy/models/login');

@injectable()
export class UserService implements IUserService {
  login: any;
  mongoUserDocument: mongoose.Model<any>;
  mongoTokenDocument: mongoose.Model<any>;
  mongoRefreshTokenDocument: mongoose.Model<any>;

  constructor(
    @inject('ISequelizeService') private sequelizeService: ISequelizeService,
    @inject('IJwtService') private jwtService: IJwtService,
  ) {
    this.login = loginModel(
      this.sequelizeService.getSequelize(),
      sequelize.DataTypes,
    );
    this.mongoUserDocument = userModel;
    this.mongoTokenDocument = tokenModel;
    this.mongoRefreshTokenDocument = refreshTokenModel;
  }

  async getAll() {
    const users = await this.login.findAll();
    return users;
  }

  async create(payload: INewAccount) {
    const newUser = await this.login.findOrCreate({
      defaults: payload,
      where: { userid: payload.userid },
    });
    await this.createMongoUser(newUser[0]);
    return newUser;
  }

  verifyNewAccountPayload(payload: any): INewAccount {
    let verifiedPayload: INewAccount = null;
    const decodedPayload = this.jwtService.decode({ token: payload.payload });
    verifiedPayload = {
      userid: decodedPayload.username,
      user_pass: decodedPayload.password,
      email: decodedPayload.email,
      sex: decodedPayload.gender,
      birthdate: new Date(decodedPayload.birthdate),
    };
    return verifiedPayload;
  }

  async createMongoUser(newUser: any) {
    const userid = newUser.getDataValue('userid');
    const accountId = newUser.getDataValue('account_id');
    const b64AccountId = Buffer.from(String(accountId), 'utf-8').toString('base64');
    const newMongoUser = await this.mongoUserDocument.findOneAndUpdate(
      {
        userid,
        mysqlReference: b64AccountId,
      },
      {
        $set: {
          userid,
          mysqlReference: b64AccountId,
        },
      },
      {
        upsert: true,
        useFindAndModify: false,
      },
    ).exec();
  }

  async onLogin(payload: any) {
    let loginPayload: ILoginPayload;
    const decodedPayload = this.jwtService.decode({ token: payload.payload });
    loginPayload = {
      userid: decodedPayload.username,
      password: decodedPayload.password,
    };
    const foundUser = await this.verifyUser(loginPayload);
    if (!foundUser) {
      throw new Error('Login fail, wrong username or password');
    }
    const refreshToken = this.jwtService.generateRefreshToken({ payload: {
      userid: foundUser.userid,
    } });
    const loginToken = this.jwtService.encode({ payload: {
      userid: foundUser.userid,
    } });
    const mongoUser = await this.mongoUserDocument.findOne({
      userid: foundUser.userid,
    }).exec();
    await this.mongoRefreshTokenDocument.findOneAndUpdate(
      {
        userRef: mongoUser,
      },
      {
        $set: {
          refreshToken,
          userRef: mongoUser,
          createdAt: Date.now(),
        },
      },
      {
        upsert: true,
        useFindAndModify: false,
      },
    );
    await this.mongoTokenDocument.findOneAndUpdate(
      {
        userRef: mongoUser,
      },
      {
        $set: {
          userRef: mongoUser,
          token: loginToken,
          createdAt: Date.now(),
        },
      },
      {
        upsert: true,
        useFindAndModify: false,
      },
    ).exec();
    const refreshTokenInstance = await this.mongoRefreshTokenDocument.findOne({
      userRef: mongoUser,
    }).exec();
    const tokenInstance = await this.mongoTokenDocument.findOne({
      userRef: mongoUser,
    }).exec();
    return {
      'token-type': 'Bearer',
      'refresh-token': refreshTokenInstance.refreshToken,
      'access-token': tokenInstance.token,
      'created-at': new Date(),
    };
  }

  async getAccessToken(refreshToken: string): Promise<IAccessToken> {
    const userInstance = await this.verifyRefreshToken(refreshToken);
    if (!userInstance) {
      throw new Error('Invalid Refresh Token');
    }
    console.log(userInstance);
    const loginToken = this.jwtService.encode({ payload: {
      userid: userInstance.userRef.userid,
    }});
    console.log(loginToken);
    await this.mongoTokenDocument.findOneAndUpdate(
      {
        userRef: userInstance,
      },
      {
        $set: {
          userRef: userInstance,
          token: loginToken,
          createdAt: Date.now(),
        },
      },
      {
        upsert: true,
        useFindAndModify: false,
      },
    ).exec();
    const tokenInstance = await this.mongoTokenDocument.findOne({
      userRef: userInstance,
    }).exec();
    return {
      'token-type': TOKEN_TYPE.BEARER,
      'access-token': tokenInstance.token,
      'created-at': new Date(),
    };
  }

  async verifyAccessToken(accessToken: string): Promise<boolean> {
    const foundAccessToken = await this.mongoTokenDocument.findOne(
      {
        token: accessToken,
      },
    ).exec();
    if (foundAccessToken) {
      return true;
    }
    return false;
  }

  async verifyRefreshToken(refreshToken: string) {
    const userid = this.jwtService.decode({
      token: refreshToken,
      secret: accessEnv('REFRESH_TOKEN_SECRET'),
    })?.userid;
    if (!userid) {
      throw new Error('Invalid Refresh Token');
    }
    const userInstance = await this.mongoRefreshTokenDocument.findOne(
      {
        'userRef.userid': userid,
      },
    ).exec();
    if (userInstance && refreshToken === userInstance.refreshToken) {
      return userInstance;
    }
    return false;
  }

  async verifyUser(loginPayload: ILoginPayload) {
    const foundUser = await this.login.findOne({
      where: {
        userid: loginPayload.userid,
      },
      attributes: [
        'userid',
        'user_pass',
      ],
    });
    if (!foundUser) {
      return false;
    }
    if (loginPayload.password !== foundUser.get('user_pass')) {
      return false;
    }
    const userPayload = JSON.parse(JSON.stringify(foundUser));
    delete userPayload.user_pass;
    return userPayload;
  }

  async findUserInformation(userid: string) {
    const foundUser = await this.login.findOne({
      where: {
        userid,
      },
      attributes: [
        'userid',
        'sex',
        'email',
        'group_id',
        'logincount',
        'lastlogin',
        'last_ip',
        'birthdate',
        'character_slots',
        'vip_time',
      ],
    });
    if (!foundUser) {
      throw new Error('No user found');
    }
    return JSON.parse(JSON.stringify(foundUser));
  }

  async findMongoUser(userid: string) {
    const foundUser = await this.mongoUserDocument.findOne({
      userid,
    });
    if (!foundUser) {
      throw new Error('User not found');
    }
    return foundUser;
  }
}
