import { injectable } from 'inversify';
import * as jwt from 'jsonwebtoken';
import { IJwtService } from '#types/jwt';
import { accessEnv } from '#helpers/accessEnv';

@injectable()
export class JwtService implements IJwtService {
  constructor() {}

  decode({ token, secret = 'latte' }: { token: string; secret?: string; }): string | any {
    return jwt.verify(token, secret);
  }

  encode(
    {
      payload,
      secret = 'latte',
      expireTime = (120 * 1000), // 120 seconds
    }: { payload: any; secret?: string, expireTime?: number },
  ): string | any {
    return jwt.sign(payload, secret, { expiresIn: expireTime });
  }

  generateRefreshToken({
    payload,
  }: { payload: any }): string {
    return this.encode({
      payload,
      secret: accessEnv('REFRESH_TOKEN_SECRET'),
      expireTime: (24 * 60 * 60 * 1000),
    });
  }
}
