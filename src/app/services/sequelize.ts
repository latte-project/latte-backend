import * as sequelize from 'sequelize';
import { ISequelizeService } from '#types/sequelize';
import { injectable } from 'inversify';
import { accessEnv } from '#helpers/accessEnv';

@injectable()
export class SequelizeService implements ISequelizeService {
  sqlize: sequelize.Sequelize;

  constructor() {
    this.sqlize = new sequelize.Sequelize(
      {
        port: accessEnv('MYSQL_DB_PORT'),
        dialect: 'mysql',
        define: {
          charset: 'utf8mb4',
          collate: 'utf8mb4_unicode_ci',
          timestamps: false,
        },
        database: accessEnv('MYSQL_DB_NAME'),
        username: accessEnv('MYSQL_DB_USERNAME'),
        password: accessEnv('MYSQL_DB_PASSWORD'),
        dialectOptions: {
          charset: 'utf8mb4',
          multipleStatements: true,
        },
        logging: false,
      },
    );
    console.table(`[${new Date().toLocaleString()}]\tMySQL connected`);
  }

  getSequelize(): sequelize.Sequelize {
    return this.sqlize;
  }
}
