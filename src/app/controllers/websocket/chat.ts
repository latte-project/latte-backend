import { Container } from 'inversify';
import * as WebSocket from 'ws';
import * as mongoose from 'mongoose';
import chatMessageModel from '#models/chat-message.model';
import { IUserService } from '#types/user';

export const chatWebsocketController = (wss: WebSocket.Server, container: Container) => {
  const chatMessageDocument: mongoose.Model<any> = chatMessageModel;
  const userService: IUserService = container.get<IUserService>('IUserService');

  wss.on('connection', (ws) => {
    ws.on('message', async (mes) => {
      wss.clients.forEach((client) => {
        if (client !== ws && client.readyState === WebSocket.OPEN) {
          client.send(mes);
        }
      });
      if (typeof(mes) === 'string') {
        const parsedMes = JSON.parse(mes);
        // const foundUser = await userService.findMongoUser(parsedMes.sender);
        // parsedMes.sender = foundUser;
        const newMessage = await chatMessageDocument.create(parsedMes);
        console.log('Created new message');
      }
    });
  });

};
