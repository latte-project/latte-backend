import chatMessageModel from '#models/chat-message.model';
import { Container } from 'inversify';
import * as mongoose from 'mongoose';
import * as express from 'express';

export const chatMessageController = (container: Container) => {
  const router = express.Router();
  const chatMessageDocument: mongoose.Model<any> = chatMessageModel;

  router.get('/', async (req, res) => {
    const messages = await chatMessageDocument.find({})
      .sort('-date')
      .limit(10)
      .exec();
    // console.log(messages);
    const convertedMessages = JSON.parse(JSON.stringify(messages));
    console.log(convertedMessages);
    res.status(200).json(convertedMessages);
  });

  return router;
};
