import { Container } from 'inversify';
import * as express from 'express';
import * as path from 'path';
import { IYamlService } from '#types/yaml';

export const apiEndpointController = (container: Container) => {
  const router = express.Router();
  const yamlService = container.get<IYamlService>('IYamlService');

  const apiEndpointCtx = yamlService.readYamlFile(path.resolve(
    'src', 'app', 'config', 'api-endpoint.yaml',
  ));

  router.get('/', (req, res) => {
    res.status(200).json(apiEndpointCtx);
  });

  return router;
};
