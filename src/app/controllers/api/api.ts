import { Container } from 'inversify';
import * as express from 'express';
import { userController } from './user';
import { apiEndpointController } from './api-endpoint';
import { apiContextController } from './api-context';
import { chatMessageController } from './chat-message';

export const apiController = (container: Container) => {
  const router = express.Router();

  router.use('/user', userController(container));
  router.use('/api-endpoint', apiEndpointController(container));
  router.use('/api-context', apiContextController(container));
  router.use('/chat-message', chatMessageController(container));

  return router;
};
