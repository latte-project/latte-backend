import { Container } from 'inversify';
import * as express from 'express';
import { IUserService } from '#types/user';
import { accessTokenGuard } from '#middleware/access-token-guard';
import { IJwtService } from '#types/jwt';

export const userController = (container: Container) => {
  const router = express.Router();
  const userService: IUserService = container.get<IUserService>('IUserService');
  const jwtService: IJwtService = container.get<IJwtService>('IJwtService');

  router.get('/', async (req, res) => {
    try {
      const users = await userService.getAll();
      res.status(200).json(users);
    } catch (err) {
      res.status(500).json({
        message: 'Server error',
        status: 'FAIL',
      });
    }
  });

  router.post('/register', async (req, res) => {
    try {
      const verifiedPayload = userService.verifyNewAccountPayload(req.body);
      const newUser = await userService.create(verifiedPayload);
      if (!newUser[1]) {
        res.status(200).json({
          message: 'Userid already existed',
          status: 'FAIL',
        });
      } else {
        res.status(200).json({
          message: 'Create new account success',
          status: 'OK',
        });
      }
    } catch (err) {
      res.status(400).json({
        message: err.message,
        status: 'FAIL',
      });
    }
  });

  router.post('/login', async (req, res) => {
    try {
      const tokenCtx = await userService.onLogin(req.body);
      const createdAt = tokenCtx['created-at'];
      const expireAt = new Date(createdAt);
      expireAt.setDate(expireAt.getDate() + 1);
      res.cookie('_rt', tokenCtx['refresh-token'], {
        httpOnly: true,
        sameSite: 'strict',
        expires: expireAt,
      });
      res.status(200).json(tokenCtx);
    } catch (err) {
      res.status(400).json({
        message: err.message,
        status: 'FAIL',
      });
    }
  });

  router.get('/logout', (req, res) => {
    try {
      res.cookie('_rt', '', {
        expires: new Date(),
      });
      res.status(200).json({
        status: 'OK',
        message: 'logout success',
      });
    } catch (err) {
      res.status(400).json({
        message: err.message,
        status: 'FAIL',
      });
    }
  });

  router.get('/access-token', async (req, res) => {
    try {
      const response = await userService.getAccessToken(req.cookies['_rt']);
      res.status(200).json(response);
    } catch (err) {
      res.status(400).json({
        message: err.message,
        status: 'FAIL',
      });
    }
  });

  router.get('/user-info', accessTokenGuard, async (req, res) => {
    try {
      const bearerToken = req.headers.authorization.split('Bearer ')[1];
      const decodedToken = jwtService.decode({ token: bearerToken });
      const userInfo = await userService.findUserInformation(decodedToken.userid);
      res.status(200).json({
        payload: userInfo,
        status: 'OK',
      });
    } catch (err) {
      res.status(400).json({
        message: err.message,
        status: 'FAIL',
      });
    }
  });

  return router;
};
