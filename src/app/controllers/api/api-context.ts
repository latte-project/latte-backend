import { Container } from 'inversify';
import * as express from 'express';
import { IYamlService } from '#types/yaml';
import * as path from 'path';

export const apiContextController = (container: Container) => {
  const router = express.Router();
  const yamlService = container.get<IYamlService>('IYamlService');

  const apiContext = yamlService.readYamlFile(path.resolve(
    'src', 'app', 'config', 'api-context.yaml',
  ));

  router.get('/', (req, res) => {
    res.status(200).json(apiContext);
  });

  return router;
};
