import * as express from 'express';
import { Container } from 'inversify';
import { apiController } from 'app/controllers/api/api';

export default (container: Container) => {
  const router: express.Router = express.Router();

  // * ADDING ROUTE CONTROLLER HERE
  router.use((req, res, next) => {
    res.locals.container = container;
    next();
  });

  router.use('/api', apiController(container));

  return router;
};
