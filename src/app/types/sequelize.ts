import * as sequelize from 'sequelize';

export interface ISequelizeService {
  getSequelize(): sequelize.Sequelize;
}
