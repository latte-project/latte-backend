export interface IUser {

}

export interface IUserService {
  getAll(): any;
  create(payload: any, options?: any): any;
  verifyNewAccountPayload(payload: object): INewAccount;
  onLogin(payload: any): any;
  getAccessToken(refreshToken: string): Promise<IAccessToken>;
  verifyAccessToken(accessToken: string): Promise<boolean>;
  findUserInformation(userid: string);
  findMongoUser(userid: string);
}

export interface INewAccount {
  userid: string;
  user_pass: string;
  email?: string;
  sex: AccountGender;
  birthdate: Date;
}

export interface ILoginPayload {
  userid: string;
  password: string;
}

export enum AccountGender {
  Male = 'M',
  Female = 'F',
  Undefined = 'S',
}

export interface IAccessToken {
  'token-type': TOKEN_TYPE;
  'access-token': string;
  'created-at': Date;
}

export enum TOKEN_TYPE {
  BEARER = 'Bearer',
}
