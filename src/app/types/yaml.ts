export interface IYamlService {
  readYamlFile(path: string);
}
