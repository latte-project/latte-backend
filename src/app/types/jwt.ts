export interface IJwtService {
  decode({ token, secret }: { token: string; secret?: string; }): string | any;
  encode(
    { payload, secret, expireTime }: { payload: any, secret?: string, expireTime?: number },
  ): string | any;
  generateRefreshToken({ payload }: { payload: any }): string;
}
