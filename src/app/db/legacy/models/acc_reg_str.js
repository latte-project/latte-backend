/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return acc_reg_str.init(sequelize, DataTypes);
}

class acc_reg_str extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    key: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "",
      primaryKey: true
    },
    index: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    value: {
      type: DataTypes.STRING(254),
      allowNull: false,
      defaultValue: "0"
    }
  }, {
    sequelize,
    tableName: 'acc_reg_str'
  });
  return acc_reg_str;
  }
}
