/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return guild_alliance.init(sequelize, DataTypes);
}

class guild_alliance extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    guild_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    opposition: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    alliance_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'guild_alliance'
  });
  return guild_alliance;
  }
}
