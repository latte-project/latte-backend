/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return item_db_re.init(sequelize, DataTypes);
}

class item_db_re extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    name_english: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "",
      unique: true
    },
    name_japanese: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: ""
    },
    type: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    price_buy: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    price_sell: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    weight: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    'atk:matk': {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    defence: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    range: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    slots: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    equip_jobs: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    equip_upper: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    equip_genders: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    equip_locations: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    weapon_level: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    equip_level: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    refineable: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    view: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true
    },
    script: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    equip_script: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    unequip_script: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'item_db_re'
  });
  return item_db_re;
  }
}
