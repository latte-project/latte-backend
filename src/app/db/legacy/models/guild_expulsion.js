/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return guild_expulsion.init(sequelize, DataTypes);
}

class guild_expulsion extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    guild_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: "",
      primaryKey: true
    },
    mes: {
      type: DataTypes.STRING(40),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'guild_expulsion'
  });
  return guild_expulsion;
  }
}
