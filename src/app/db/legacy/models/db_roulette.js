/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return db_roulette.init(sequelize, DataTypes);
}

class db_roulette extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    index: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    level: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    item_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    amount: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 1
    },
    flag: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 1
    }
  }, {
    sequelize,
    tableName: 'db_roulette'
  });
  return db_roulette;
  }
}
