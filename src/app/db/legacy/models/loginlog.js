/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return loginlog.init(sequelize, DataTypes);
}

class loginlog extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ip: {
      type: DataTypes.STRING(15),
      allowNull: false,
      defaultValue: ""
    },
    user: {
      type: DataTypes.STRING(23),
      allowNull: false,
      defaultValue: ""
    },
    rcode: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    log: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'loginlog'
  });
  return loginlog;
  }
}
