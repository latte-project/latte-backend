/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return homunculus.init(sequelize, DataTypes);
}

class homunculus extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    homun_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    class: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    prev_class: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    exp: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    intimacy: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    hunger: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    str: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    agi: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    vit: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    int: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    dex: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    luk: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    hp: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    max_hp: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    sp: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    max_sp: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    skill_point: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    alive: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    rename_flag: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    vaporize: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    autofeed: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'homunculus'
  });
  return homunculus;
  }
}
