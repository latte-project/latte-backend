/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return buyingstores.init(sequelize, DataTypes);
}

class buyingstores extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    sex: {
      type: DataTypes.ENUM('F','M'),
      allowNull: false,
      defaultValue: "M"
    },
    map: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    x: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    y: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    limit: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    body_direction: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "4"
    },
    head_direction: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "0"
    },
    sit: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "1"
    },
    autotrade: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'buyingstores'
  });
  return buyingstores;
  }
}
