/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return mercenary_owner.init(sequelize, DataTypes);
}

class mercenary_owner extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    merc_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    arch_calls: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    arch_faith: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    spear_calls: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    spear_faith: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    sword_calls: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    sword_faith: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'mercenary_owner'
  });
  return mercenary_owner;
  }
}
