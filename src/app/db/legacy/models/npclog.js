/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return npclog.init(sequelize, DataTypes);
}

class npclog extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    npc_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    npc_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    char_name: {
      type: DataTypes.STRING(25),
      allowNull: false,
      defaultValue: ""
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    },
    mes: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'npclog'
  });
  return npclog;
  }
}
