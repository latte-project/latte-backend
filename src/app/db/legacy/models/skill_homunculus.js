/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return skill_homunculus.init(sequelize, DataTypes);
}

class skill_homunculus extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    homun_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    lv: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'skill_homunculus'
  });
  return skill_homunculus;
  }
}
