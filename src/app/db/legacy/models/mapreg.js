/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return mapreg.init(sequelize, DataTypes);
}

class mapreg extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    varname: {
      type: DataTypes.STRING(32),
      allowNull: false,
      primaryKey: true
    },
    index: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'mapreg'
  });
  return mapreg;
  }
}
