/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return feedinglog.init(sequelize, DataTypes);
}

class feedinglog extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    target_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    target_class: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    type: {
      type: DataTypes.ENUM('P','H','O'),
      allowNull: false
    },
    intimacy: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    item_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false
    },
    x: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    y: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'feedinglog'
  });
  return feedinglog;
  }
}
