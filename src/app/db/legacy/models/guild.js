/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return guild.init(sequelize, DataTypes);
}

class guild extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    guild_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    master: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    },
    guild_lv: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    connect_member: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    max_member: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    average_lv: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 1
    },
    exp: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    next_exp: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    skill_point: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    mes1: {
      type: DataTypes.STRING(60),
      allowNull: false,
      defaultValue: ""
    },
    mes2: {
      type: DataTypes.STRING(120),
      allowNull: false,
      defaultValue: ""
    },
    emblem_len: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    emblem_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    emblem_data: {
      type: 'BLOB',
      allowNull: true
    },
    last_master_change: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'guild'
  });
  return guild;
  }
}
