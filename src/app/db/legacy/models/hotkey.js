/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return hotkey.init(sequelize, DataTypes);
}

class hotkey extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    hotkey: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    itemskill_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    skill_lvl: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'hotkey'
  });
  return hotkey;
  }
}
