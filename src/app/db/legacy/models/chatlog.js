/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return chatlog.init(sequelize, DataTypes);
}

class chatlog extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    type: {
      type: DataTypes.ENUM('O','W','P','G','M','C'),
      allowNull: false,
      defaultValue: "O"
    },
    type_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    src_charid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    src_accountid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    src_map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    },
    src_map_x: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    src_map_y: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    dst_charname: {
      type: DataTypes.STRING(25),
      allowNull: false,
      defaultValue: ""
    },
    message: {
      type: DataTypes.STRING(150),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'chatlog'
  });
  return chatlog;
  }
}
