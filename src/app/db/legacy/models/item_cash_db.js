/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return item_cash_db.init(sequelize, DataTypes);
}

class item_cash_db extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    tab: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    item_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    price: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'item_cash_db'
  });
  return item_cash_db;
  }
}
