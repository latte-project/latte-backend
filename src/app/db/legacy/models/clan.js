/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return clan.init(sequelize, DataTypes);
}

class clan extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    clan_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    },
    master: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    },
    mapname: {
      type: DataTypes.STRING(24),
      allowNull: false,
      defaultValue: ""
    },
    max_member: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'clan'
  });
  return clan;
  }
}
