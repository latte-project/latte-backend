/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return sc_data.init(sequelize, DataTypes);
}

class sc_data extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    tick: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    val1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    val2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    val3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    val4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'sc_data'
  });
  return sc_data;
  }
}
