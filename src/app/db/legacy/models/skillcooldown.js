/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return skillcooldown.init(sequelize, DataTypes);
}

class skillcooldown extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    account_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    skill: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    tick: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'skillcooldown'
  });
  return skillcooldown;
  }
}
