/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return cashlog.init(sequelize, DataTypes);
}

class cashlog extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    char_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.ENUM('T','V','P','M','S','N','D','C','A','E','I','B','$'),
      allowNull: false,
      defaultValue: "S"
    },
    cash_type: {
      type: DataTypes.ENUM('O','K','C'),
      allowNull: false,
      defaultValue: "O"
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'cashlog'
  });
  return cashlog;
  }
}
