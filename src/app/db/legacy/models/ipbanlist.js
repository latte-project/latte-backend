/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return ipbanlist.init(sequelize, DataTypes);
}

class ipbanlist extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    list: {
      type: DataTypes.STRING(15),
      allowNull: false,
      defaultValue: "",
      primaryKey: true
    },
    btime: {
      type: DataTypes.DATE,
      allowNull: false,
      primaryKey: true
    },
    rtime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    reason: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    }
  }, {
    sequelize,
    tableName: 'ipbanlist'
  });
  return ipbanlist;
  }
}
