/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return memo.init(sequelize, DataTypes);
}

class memo extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    memo_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    map: {
      type: DataTypes.STRING(11),
      allowNull: false,
      defaultValue: ""
    },
    x: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    y: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'memo'
  });
  return memo;
  }
}
