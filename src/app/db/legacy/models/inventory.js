/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return inventory.init(sequelize, DataTypes);
}

class inventory extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    nameid: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    amount: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    equip: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    identify: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    refine: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    attribute: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card0: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card1: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card2: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card3: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    option_id0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    expire_time: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    favorite: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    bound: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    unique_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    equip_switch: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'inventory'
  });
  return inventory;
  }
}
