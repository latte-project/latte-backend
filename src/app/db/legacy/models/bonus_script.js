/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return bonus_script.init(sequelize, DataTypes);
}

class bonus_script extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    script: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    tick: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    flag: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    icon: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: -1
    }
  }, {
    sequelize,
    tableName: 'bonus_script'
  });
  return bonus_script;
  }
}
