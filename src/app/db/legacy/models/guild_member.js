/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return guild_member.init(sequelize, DataTypes);
}

class guild_member extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    guild_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    char_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      primaryKey: true
    },
    exp: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    position: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'guild_member'
  });
  return guild_member;
  }
}
