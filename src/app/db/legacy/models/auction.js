/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return auction.init(sequelize, DataTypes);
}

class auction extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  super.init({
    auction_id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    seller_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    seller_name: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: ""
    },
    buyer_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    buyer_name: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: ""
    },
    price: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    buynow: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    hours: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    timestamp: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    nameid: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    item_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: ""
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    refine: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    attribute: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card0: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card1: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card2: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    card3: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    option_id0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm0: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_id4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_val4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    option_parm4: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    unique_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'auction'
  });
  return auction;
  }
}
