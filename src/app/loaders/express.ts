import * as express from 'express';
import * as compression from 'compression';
import * as cors from 'cors';
import * as multer from 'multer';
import * as swaggerUiExpress from 'swagger-ui-express';
import * as yaml from 'js-yaml';
import * as fs from 'fs';
import * as path from 'path';
import * as helmet from 'helmet';
import * as cookieParser from 'cookie-parser';

import { Container } from 'inversify';
import router from '#routes/router';

export const expressLoader = async (
  { container, app }:  { container: Container, app: express.Express },
) => {
  const upload = multer();
  const swaggerDoc = loadConfig(path.resolve('src', 'app', 'config', 'swagger.yaml'));
  const whitelist = ['http://localhost:4200'];
  const corsOptions = {
    origin: (origin, callback) => {
      if (whitelist.indexOf(origin) !== -1 || !origin) {
        callback(null, true);
      } else {
        callback(new Error('Not Allow by CORS'));
      }
    },
    credentials: true,
  };

  app.use(helmet());
  app.use(cors(corsOptions));
  app.use(compression());
  app.use(express.json());
  app.use(express.raw());
  app.use(express.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(upload.array('uploadingFiles'));
  app.use(express.static(path.join(__dirname, 'public')));

  app.get('/status', (req, res) => { res.status(200).end(); });
  app.get('/hello-world', (req, res) => {
    res.json('Hello World');
  });

  app.use('/api-docs', swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDoc));

  app.use('/', router(container));
  return app;
};

export const loadConfig = (path: any) => {
  try {
    const swaggerFile = fs.readFileSync(path, 'utf-8');
    const swaggerYaml = yaml.safeLoad(swaggerFile);
    return JSON.parse(JSON.stringify(swaggerYaml));
  } catch (err) {
    throw new Error('loadConfig error');
  }
};
