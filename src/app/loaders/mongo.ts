import * as mongoose from 'mongoose';
import { accessEnv } from '#helpers/accessEnv';

export const mongoLoader = async () => {
  const mongoDbUrl: string = accessEnv('MONGO_DB_URL', 'mongodb://localhost:27017/latte');
  const connection = await mongoose.connect(
    mongoDbUrl,
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  );
  return connection.connection.db;
};
