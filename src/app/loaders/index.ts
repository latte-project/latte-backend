import { iocLoader } from './ioc';
import { mongoLoader } from './mongo';
import { expressLoader } from './express';
import { websocketLoader } from './websocket';

export default async ({ expressApp }) => {
  const container = iocLoader();
  websocketLoader(container);
  await mongoLoader();
  console.table(`[${new Date().toLocaleString()}]\tMongoDB connected`);
  await expressLoader({ container, app: expressApp });
  console.table(`[${new Date().toLocaleString()}]\tExpress application initialized`);
};
