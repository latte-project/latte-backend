import { Container } from 'inversify';
import { ISequelizeService } from '#types/sequelize';
import { SequelizeService } from '#services/sequelize';
import { IUserService } from '#types/user';
import { UserService } from '#services/user';
import { IJwtService } from '#types/jwt';
import { JwtService } from '#services/jwt';
import { IYamlService } from '#types/yaml';
import { YamlService } from '#services/yaml';

export const iocLoader = () => {
  const container: Container = new Container();

  container.bind<ISequelizeService>('ISequelizeService').to(SequelizeService).inSingletonScope();
  container.bind<IUserService>('IUserService').to(UserService);
  container.bind<IJwtService>('IJwtService').to(JwtService);
  container.bind<IYamlService>('IYamlService').to(YamlService);

  return container;
};
