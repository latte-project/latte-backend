import 'reflect-metadata';
import * as express from 'express';
import { accessEnv } from '#helpers/accessEnv';
import loaders from './loaders';

export class App {
  app: express.Express;

  constructor() {
    this.app = express();
  }

  async start(): Promise<void> {
    const PORT = accessEnv('PORT', 4001);
    await loaders({ expressApp: this.app });

    this.app.listen(PORT, (err: Error) => {
      if (err) {
        console.table(err);
        return;
      }
      console.log(`[${new Date().toLocaleString()}]\tServer started and listen on PORT: ${process.env.PORT}`);
    });
  }
}
