import * as mongoose from 'mongoose';
import { userSchema } from './user.model';

const Schema = mongoose.Schema;

const opts = { toJSON: { virtuals: true } };
const chatMessageSchema = new Schema(
  {
    date: {
      type: Date,
      required: true,
    },
    dateFormat: {
      type: String,
      default: 'HH:mm MMM dd yyyy',
    },
    latitude: {
      type: Number,
      default: null,
    },
    longitude: {
      type: Number,
      default: null,
    },
    message: {
      type: String,
      required: true,
    },
    quote: {
      type: Boolean,
      default: false,
    },
    sender: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      default: 'text',
    },
    avatar: {
      type: String,
      required: false,
    },
  },
  opts,
);

export default mongoose.model('ChatMessage', chatMessageSchema);
