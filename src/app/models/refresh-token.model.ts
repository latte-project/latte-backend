import * as mongoose from 'mongoose';
import { userSchema } from './user.model';

const Schema = mongoose.Schema;

const opts = { toJSON: { virtuals: true } };
const refreshTokenSchema = new Schema(
  {
    userRef: {
      type: userSchema,
      required: true,
    },
    refreshToken: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      expires: '1d',
      default: Date.now,
    },
  },
  opts,
);

export default mongoose.model('RefreshToken', refreshTokenSchema);
