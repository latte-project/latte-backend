import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const opts = { toJSON: { virtuals: true } };
export const userSchema = new Schema(
  {
    userid: {
      type: String,
      required: true,
      unique: true,
    },
    mysqlReference: {
      type: String,
      required: true,
      unique: true,
    },
    meta: {
      type: Schema.Types.Mixed,
      default: null,
    },
  },
  opts,
);

export default mongoose.model('User', userSchema);
