import * as mongoose from 'mongoose';
import { userSchema } from './user.model';

const Schema = mongoose.Schema;

const opts = { toJSON: { virtuals: true } };
const tokenSchema = new Schema(
  {
    userRef: {
      type: userSchema,
      required: true,
    },
    token: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      expires: '2m',
      default: Date.now,
    },
  },
  opts,
);

export default mongoose.model('Token', tokenSchema);
