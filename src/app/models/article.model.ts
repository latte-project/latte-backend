import * as mongoose from 'mongoose';
import { userSchema } from './user.model';

const Schema = mongoose.Schema;

const opts = { toJSON: { virtuals: true } };
export const articleSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      unique: true,
    },
    author: {
      type: userSchema,
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
    banner: {
      type: String,
    },
    visible: {
      type: Boolean,
      default: true,
    },
    tags: {
      type: Schema.Types.Mixed,
      default: {
        news: false,
        hot: false,
        event: false,
        guide: false,
        etc: true,
      },
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    updatedAt: {
      type: Date,
      default: Date.now,
    },
  },
  opts,
);

export default mongoose.model('Article', articleSchema);
