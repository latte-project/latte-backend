import { IUserService } from '#types/user';

export const accessTokenGuard = async (req, res, next) => {
  const userService: IUserService = res.locals.container.get('IUserService');

  const bearerToken =
    req.headers.authorization ? req.headers.authorization.split('Bearer ')[1] : null;

  const verified = await userService.verifyAccessToken(bearerToken);

  if (verified) {
    next();
  } else {
    res.status(404).json({
      message: 'Unauthorized',
      status: 'FAIL',
    });
    throw new Error('Unauthorized');
  }

};
